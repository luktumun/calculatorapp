import React, { Component } from "react";
import "../styles.css";
import "./KeyPad.css";
class KeyPadComponent extends Component {
  render() {
    let { result } = this.props;
    const btnValues = [
      { item: "C" },
      { item: "CE" },
      { item: "%" },
      { item: "/" },
      { item: "*" },
      { item: "-" },
      { item: "+" },

      { item: 9 },
      { item: 8 },
      { item: 7 },

      { item: 6 },
      { item: 5 },
      { item: 4 },

      { item: 3 },
      { item: 2 },
      { item: 1 },

      { item: 0 },
      { item: "." },
      { item: "=" },
    ];
    const btn1 = btnValues.map((btn) => (
      <button
        name={btn.item}
        onClick={(e) => this.props.onClick(e.target.name)}
        key={btn.item}
      >
        {btn.item}
      </button>
    ));
    return (
      <div>
        <input className="input" type="text" value={result} />

        <ul>{btn1}</ul>
      </div>
    );
  }
}

export default KeyPadComponent;
